# srrg_scan_matcher_txtio

This package allows to perform scan matching and 2D pose tracking from files in txt_io format.

## Prerequisites

This package requires:
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_boss](https://gitlab.com/srrg-software/srrg_boss)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_viewers](https://gitlab.com/srrg-software/srrg_core_viewers)
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers)

## Applications

* `srrg_scan_matcher_txtio`: performs scan matching and pose tracking from a txt_io file and outputs a new txt_io with the computed poses. Run it as a ros node by:

```sh
$ rosrun srrg_scan_matcher_txtio srrg_scan_matcher_txtio <input_file_in_txtio.txt>
```
Use parameter `-h` for program options.