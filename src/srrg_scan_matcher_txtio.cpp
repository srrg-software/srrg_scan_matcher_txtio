//scan matcher things
#include "projective_correspondence_finder2d.h"
#include "laser_message_tracker_trigger.h"

//g2o stuff
#include "g2o/stuff/command_args.h"

//srrg core
#include <srrg_messages/base_sensor_message.h>
#include <srrg_messages/message_reader.h>
#include <srrg_messages/message_writer.h>

//interface
#include <QApplication>
#include "tracker_viewer.h"

using namespace std;
using namespace g2o;
using namespace srrg_core;
using namespace srrg_scan_matcher;
using namespace srrg_scan_matcher_gui;

double inlier_distance;
int iterations;
int frame_skip;
double bpr;
double min_correspondences_ratio;
double local_map_clipping_range;
double local_map_clipping_translation_threshold;
double max_matching_range;
double min_matching_range;
int num_matching_beams;
double merging_distance, merging_normal_angle;
double voxelize_res;
double matching_fov;

bool use_gui;
bool projective_merge;
std::string dump;

double tracker_damping;
std::string cfinder;
bool verbose;

double laser_translation_threshold;
double laser_rotation_threshold;

std::vector<int> odom_weights;
int default_odom_weights[] = {100,50,100};
std::string dumpFilename, outputFilename;

class LaserMessageTrackerDumperTrigger: public LaserMessageTrackerTrigger{
 public:
  LaserMessageTrackerDumperTrigger(SensorMessageSorter* sorter, 
				   Tracker2D* tracker, 
				   MessageWriter* writer) : 
     LaserMessageTrackerTrigger(sorter, tracker){
    _writer = writer;
  }

  void action(std::tr1::shared_ptr<BaseSensorMessage> msg){
    LaserMessageTrackerTrigger::action(msg);
    Eigen::Isometry2f global_t = getGlobalTransform();
    Eigen::Isometry3f odometry = Eigen::Isometry3f::Identity();
    odometry.translation().x() = global_t.translation().x();
    odometry.translation().y() = global_t.translation().y();
    odometry.linear().block<2,2>(0,0) = global_t.linear();
    msg->setOdometry(odometry);

    _writer->writeMessage(*msg);
  } 

 protected:
  MessageWriter* _writer;

};

void readParameters(int argc, char **argv){

  CommandArgs arg;

  arg.param("inlier_distance", inlier_distance, 0.3, "tracker inlier distance");
  arg.param("iterations", iterations, 10, "tracker iterations");
  arg.param("frame_skip", frame_skip, 1, "use one scan every <frame_skip> scans");
  arg.param("bpr", bpr, 0.4, "tracker bad points ratio");
  arg.param("min_correspondences_ratio", min_correspondences_ratio, 0.5, "tracker minimum correspondences ratio");
  arg.param("local_map_clipping_range", local_map_clipping_range, 10.0, "tracker local map clipping range");
  arg.param("local_map_clipping_translation_threshold", local_map_clipping_translation_threshold, 5.0, "tracker local map clipping translation threshold");
  arg.param("max_matching_range", max_matching_range, 0.0, "tracker max matching range");
  arg.param("min_matching_range", min_matching_range, 0.0, "tracker min matching range");
  arg.param("num_matching_beams", num_matching_beams, 0, "tracker num matching beams");
  arg.param("merging_distance", merging_distance, 0.2, "merging distance");
  arg.param("merging_normal_angle", merging_normal_angle, 1.0, "merging_normal_angle");
  arg.param("voxelize_res", voxelize_res, 0.0, "voxelization resolution");
  arg.param("matching_fov", matching_fov, 0.0, "matching field of view");

  arg.param("use_gui", use_gui, false, "displays the GUI");
  arg.param("projective_merge", projective_merge, true, "use projective merge");
  arg.param("dump", dump, std::string(""), "filename to save the dumps generated");

  arg.param("tracker_damping", tracker_damping, 1.0, "tracker damping factor");
  arg.param("cfinder", cfinder, std::string("projective"), "choose between projective or nn correspondence finder");
  arg.param("verbose", verbose, false, "display execution info");

  arg.param("laser_translation_threshold", laser_translation_threshold, -1., "translation threshold to process a new laser scan (-1=process all)");
  arg.param("laser_rotation_threshold", laser_rotation_threshold, -1., "orientation threshold to process a new laser scan (-1=process all)");

  arg.param("odom_weights", odom_weights, std::vector<int>(), "weights for odometry (x,y,theta) separated by commas without spaces \n  e.g: 10,5,10 ");

  arg.param("o", outputFilename, "out.txt", "file where to save the output of the tracker");
  arg.paramLeftOver("dump-file", dumpFilename, "", "input dump file in txt io format");
  arg.parseArgs(argc, argv);

  if (frame_skip <= 0)
    frame_skip = 1;

  std::cout << "Launched with params: " << std::endl;
  std::cout << "inlier_distance " << inlier_distance << std::endl;
  std::cout << "iterations " << iterations << std::endl;
  std::cout << "frame_skip " << frame_skip << std::endl;
  std::cout << "bpr " << bpr << std::endl;
  std::cout << "min_correspondences_ratio " << min_correspondences_ratio << std::endl;
  std::cout << "local_map_clipping_range " << local_map_clipping_range << std::endl;
  std::cout << "local_map_clipping_translation_threshold " << local_map_clipping_translation_threshold << std::endl;
  std::cout << "max_matching_range " << max_matching_range << std::endl;
  std::cout << "min_matching_range " << min_matching_range << std::endl;
  std::cout << "num_matching_beams " << num_matching_beams << std::endl;
  std::cout << "merging_distance " << merging_distance << std::endl;
  std::cout << "merging_normal_angle " << merging_normal_angle << std::endl;
  std::cout << "voxelize_res " << voxelize_res << std::endl;
  std::cout << "matching_fov " << matching_fov << std::endl;
  std::cout << "use_gui " << (use_gui?"true":"false") << std::endl;
  std::cout << "projective_merge " << (projective_merge?"true":"false") << std::endl;
  std::cout << "dump " << dump << std::endl;
  std::cout << "tracker_damping " << tracker_damping << std::endl;
  std::cout << "cfinder " << cfinder << std::endl;
  std::cout << "verbose " << verbose << std::endl;

  std::cout << "laser_translation_threshold " << laser_translation_threshold << std::endl;
  std::cout << "laser_rotation_threshold " << laser_rotation_threshold << std::endl;

  if (!odom_weights.size()){
    odom_weights = std::vector<int>(default_odom_weights, default_odom_weights + sizeof(default_odom_weights) / sizeof(int));
  }

  std::cout << "odom_weights ";
  for (size_t i=0; i< odom_weights.size(); i++){
    std::cout << odom_weights[i];
    if (i < odom_weights.size()-1)
      std::cout << ",";
  }
  std::cout << std::endl;

  fflush(stdout);

}

Projector2D* projector = 0;
Tracker2D* tracker = new Tracker2D(); 
ProjectiveCorrespondenceFinder2D* projective_finder = new ProjectiveCorrespondenceFinder2D;
NNCorrespondenceFinder2D* nn_finder = new NNCorrespondenceFinder2D;

int main(int argc, char **argv){
  if (argc<2) {
    std::cerr << "srrg_scan_matcher_txtio: runs the scan_matcher with the laserscans of a dump file written with txt_io format" << std::endl;
    std::cerr << "Error: you must provide some parameter. Use 'srrg_scan_matcher_txtio -h' for help. " << std::endl;
    return 0;
  }

  readParameters(argc,argv);

  if (dumpFilename == outputFilename){
    std::cerr << "Error: use a different name for the output file (-o option)" << std::endl;
    return -1;
  }
  
  // Setting input parameters
  if (cfinder=="projective")
    tracker->setCorrespondenceFinder(projective_finder);
  else if (cfinder=="nn")
    tracker->setCorrespondenceFinder(nn_finder);
  else {
    std::cerr << "unknown correspondence finder type: [" << cfinder << "]" << std::endl;
    return -1;
  }

  tracker->setInlierDistance(inlier_distance);
  tracker->setIterations(iterations);
  tracker->setBpr(bpr);
  tracker->setMinCorrespondencesRatio(min_correspondences_ratio);
  tracker->setLocalMapClippingRange(local_map_clipping_range);
  tracker->setClipTranslationThreshold(local_map_clipping_translation_threshold);
  tracker->setMergingDistance(merging_distance);
  tracker->setMergingNormalAngle(merging_normal_angle);
  tracker->setVoxelizeResolution(voxelize_res);
  tracker->setEnableProjectiveMerge(projective_merge);
  tracker->setDumpFilename(dump);
  tracker->solver()->setDamping(tracker_damping);
  tracker->setVerbose(verbose);

  std::cerr << "Tracker2D allocated" << std::endl;

  QApplication* app=0;
  Tracker2DViewer* tviewer=0;
  if (use_gui) {
    app=new QApplication(argc, argv);
    tviewer=new Tracker2DViewer(tracker);
    tviewer->init();
    tviewer->show();
  }
  
  MessageReader reader;
  reader.open(dumpFilename);
  if (!reader.good()){
    std::cerr << "Failed openning file: " << dumpFilename << std::endl;
    return -1;
  }

  MessageWriter* writer = new MessageWriter;
  writer->open(outputFilename);

  SensorMessageSorter* sorter = new SensorMessageSorter;
  sorter->setWriteBackEnabled(false);

  LaserMessageTrackerDumperTrigger* lmtt = new LaserMessageTrackerDumperTrigger(sorter, tracker, writer);
  lmtt->setFrameSkip(frame_skip);
  
  lmtt->setMaxMatchingRange(max_matching_range);
  lmtt->setMinMatchingRange(min_matching_range);
  lmtt->setNumMatchingBeams(num_matching_beams);
  lmtt->setMatchingFov(matching_fov);
  lmtt->setOdomWeights(odom_weights[0], odom_weights[1], odom_weights[2]);
  lmtt->setLaserTranslationThreshold(laser_translation_threshold);
  lmtt->setLaserRotationThreshold(laser_rotation_threshold);

  BaseMessage* msg=0;
  while (msg = reader.readMessage()) {
    BaseSensorMessage* sensor_msg = dynamic_cast<BaseSensorMessage*>(msg);
    sorter->insertMessage(sensor_msg);
  
    if (use_gui) {
      tviewer->updateGL();
      app->processEvents();
      QKeyEvent* event=tviewer->lastKeyEvent();
      if (event) {
	if (event->key()==Qt::Key_F) {
	  tviewer->setFollowRobotEnabled(!tviewer->followRobotEnabled());
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_B) {
	  tviewer->setDrawRobotEnabled(!tviewer->drawRobotEnabled());
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_R) {
	  tracker->reset();
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_H) {
	  std::cerr << "HELP" << std::endl;
	  std::cerr << "R: reset tracker" << std::endl;
	  std::cerr << "B: draws robot" << std::endl;
	  std::cerr << "F: enables/disables robot following" << std::endl;
	  tviewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_P) {//PAUSE
	  tviewer->keyEventProcessed();
	  std::cerr << "PAUSED. Press key 'P' to continue." << std::endl;
	  while (true){
	    app->processEvents();
	    event=tviewer->lastKeyEvent();
	    if (event && event->key()==Qt::Key_P){
	      tviewer->keyEventProcessed();
	      break;
	    }
	    usleep(100000);
	  }
	}
      }
    }
  }

  sorter->flush();
}
