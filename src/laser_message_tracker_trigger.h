#pragma once 
#include <srrg_messages/laser_message.h>
#include <srrg_messages/sensor_message_sorter.h>

#include "tracker2d.h"

using namespace srrg_core;
using namespace srrg_scan_matcher;

class LaserMessageTrackerTrigger: public SensorMessageSorter::Trigger{
 public:
  LaserMessageTrackerTrigger(SensorMessageSorter* sorter, Tracker2D* tracker);

  inline void setFrameSkip(int frame_skip){_frame_skip = frame_skip;}

  inline void setMaxMatchingRange(double max_matching_range){_max_matching_range = max_matching_range;}
  inline void setMinMatchingRange(double min_matching_range){_min_matching_range = min_matching_range;}
  inline void setNumMatchingBeams(int num_matching_beams){_num_matching_beams = num_matching_beams;}
  inline void setMatchingFov(double matching_fov){_matching_fov = matching_fov;}
  
  inline void setOdomWeights(float wx, float wy, float wtheta) {_odom_weights << wx, wy, wtheta; }

  inline void setLaserTranslationThreshold(double laser_translation_threshold) {_laser_translation_threshold = laser_translation_threshold;}
  inline void setLaserRotationThreshold(double laser_rotation_threshold) {_laser_rotation_threshold = laser_rotation_threshold;}

  void action(std::tr1::shared_ptr<BaseSensorMessage> msg);

  inline Eigen::Isometry2f getGlobalTransform() {return _tracker->globalT();}

 protected:
  
  void initializeProjectorFromParameters(LaserMessage* las);

  int _frame_count;
  int _frame_skip;
  bool _has_guess;

  double _max_matching_range;
  double _min_matching_range;
  int _num_matching_beams;
  double _matching_fov;

  Eigen::Vector3f _odom_weights;

  double _laser_translation_threshold;
  double _laser_rotation_threshold;

  Projector2D* _projector;
  Tracker2D* _tracker;

  Eigen::Isometry2f _previous_odom;
};

